<?php

namespace Modules\ShoppingFeed\Controllers;

use App\Helpers\ApiClient;
use App\Utils;
use Illuminate\Http\Request;
use Megaads\ApifyClient\Client;
use Modules\ShoppingFeed\Controllers\Controller;
use Module;

class ShoppingFeedController extends Controller
{
    private $baseUrl;
    private $shopName;

    private $today;

    public function __construct()
    {
        Module::onView("content", function() {
            return "This is content view from ShoppingFeed Module HomeController";
        }, 5);

        $this->today = date('Y-m-d');
        $this->baseUrl = URL('/');
        $shopName = explode('.', request()->getHttpHost());
        if(isset($shopName[0])){
            $this->shopName = $shopName[0];
        }else{
            $this->shopName = 'shopbay';
        }
    }
    public function index(Request $request)
    {
        $message = config("shopping-feed::app.message");
        return view('shopping-feed::home.welcome', [
            'message' => $message,
        ]);
    }

    public function getGoogleShoppingFeed($idFeedSetting, $feedFileName){
        $dirPath = "shopping-feed/$this->shopName/$this->today";
        $filePath = "shopping-feed/$this->shopName/$this->today/$idFeedSetting.xml";

        if(!file_exists($dirPath)){
            $this->generateXmlFeed($idFeedSetting, $feedFileName);
        }
        if(!file_exists($filePath)){

            $query = ApiClient::buildClient("settingshoppingfeed");
            $query->filter("id", Client::SELECTION_EQUAL, $idFeedSetting);
            $query->pageSize(-1);
            $result = $query->first();

            if ($result['status'] == 'successful') {
                $this->generateXmlFeed($idFeedSetting, $feedFileName);
            }else{
                return response('404 Không tìm thấy dữ liệu!', 404);
            }


        }

        return response()->file($filePath);

    }

    public function generateXmlFeed($idFeedSetting, $feedFileName)
    {
        $dirPath = "shopping-feed/$this->shopName/$this->today";
        if (!file_exists($dirPath)) {
            mkdir($dirPath ,0777, true);
        }

        $query = ApiClient::buildClient("settingshoppingfeed");
        $query->filter("id", Client::SELECTION_EQUAL, $idFeedSetting);
        $query->pageSize(-1);
        $result = $query->first();

        if ($result['status'] == 'successful') {
            $setting = $result['result'];

            if (empty($setting)) {
                return response('404 Không tìm thấy dữ liệu!', 404);
            }

            // lấy danh sách sản phẩm
            $productFeed = ApiClient::buildCustomRequest("product/feed", 'GET', ['setting' => $setting], []);

            $filePath = realpath($dirPath) . '/' .  $idFeedSetting . '.xml';
            $indexwriter = new \XMLWriter();
            $indexwriter->openURI($filePath);
            $indexwriter->startDocument('1.0', 'UTF-8');
            $indexwriter->setIndent(true);
            $indexwriter->startElement('rss');
            $indexwriter->writeAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
            $indexwriter->writeAttribute('version', '2.0');

            $indexwriter->startElement('channel');
            $indexwriter->writeElement('title', $productFeed['channel']['general.store_name']);
            $indexwriter->writeElement('link', $this->baseUrl);
            $indexwriter->writeElement('description', $productFeed['channel']['general.about']);

            /* Sản phẩm */
            foreach ($productFeed['items'] as $item) {
                if (isset($item['products_sku']) && count($item['products_sku']) > 0) {
                    foreach ($item['products_sku'] as $sku) {
                        $indexwriter->startElement('item');
                        $indexwriter->writeElement('g:id', $sku['sku']);
                        $indexwriter->writeElement('g:title', ($setting['is_variant_title']) ? $sku['product_name'] : $item['name']);
                        $indexwriter->writeElement('g:description', trim(strip_tags($item['description'])));
                        $indexwriter->writeElement('g:link', $this->baseUrl . $item['url'] . '?spid=' . $sku['id']);
                        $indexwriter->writeElement('g:image_link', $sku['image_url']);
                        $indexwriter->writeElement('g:condition', "new");
                        $indexwriter->writeElement('g:availability', "in stock");
                        $indexwriter->writeElement('g:price', doubleval($item['price']) . ' VND');

                        if(isset($productFeed['taxonomy_category']) && $productFeed['taxonomy_category']){
                            $indexwriter->writeElement('g:google_product_category', $productFeed['taxonomy_category']['product_taxonomy_id']);
                        }

                        if (isset($item['brand']))
                            $indexwriter->writeElement('g:brand', $item['brand']['name']);
                        $indexwriter->endElement();
                    }

                } else {
                    $indexwriter->startElement('item');
                    $indexwriter->writeElement('g:id', $item['sku']);
                    $indexwriter->writeElement('g:title', $item['name']);
                    $indexwriter->writeElement('g:description', trim(strip_tags($item['description'])));
                    $indexwriter->writeElement('g:link', $this->baseUrl . $item['url']);
                    $indexwriter->writeElement('g:image_link', $item['image_url']);
                    $indexwriter->writeElement('g:condition', "new");
                    $indexwriter->writeElement('g:availability', "in stock");
                    $indexwriter->writeElement('g:price', doubleval($item['price']) . ' VND');

                    if(isset($productFeed['taxonomy_category']) && $productFeed['taxonomy_category']){
                        $indexwriter->writeElement('g:google_product_category', $productFeed['taxonomy_category']['product_taxonomy_id']);
                    }

                    if (isset($item['brand']))
                        $indexwriter->writeElement('g:brand', $item['brand']['name']);
                    $indexwriter->endElement();
                }
            }

            $indexwriter->endElement(); // channel

            $indexwriter->endElement(); // rss
            $indexwriter->endDocument();
        }

        return true;
    }
}

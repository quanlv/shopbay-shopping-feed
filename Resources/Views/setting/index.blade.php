@section('script')
<script src="/system/js/controllers/pagination/pagination-controller.js" charset="utf-8"></script>
<script src="/system/js/controllers/shopping-feed/shopping-feed-list-controller.js?v=<?= Config::get("sa.version") ?>" charset="utf-8"></script>
@endsection
<style>
    .s-tag {
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        background-color: #f5f6f7;
        border-radius: 3px;
        color: #535d65;
        display: -webkit-inline-box;
        display: -ms-inline-flexbox;
        display: inline-flex;
        font-size: 1.5rem;
        height: 2em;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        line-height: 1.5;
        padding-left: .75em;
        padding-right: .75em;
        white-space: nowrap;
        width: -webkit-max-content;
        width: -moz-max-content;
        width: max-content;
        max-width: 100%;
    }
</style>
<div class="content" ng-controller="ShoppingFeedListController" ng-cloak>
    <div class="header" ng-show="mode == 'list'">
        <div class="pull-left">
            <h3 class="">Danh sách feed</h3>
        </div>
        <button type="button" name="button" class="btn btn-success btn-flat pull-right add" ng-click="create();mode = 'create'">Thêm feed</button>
        <div class="clearfix">
        </div>
    </div>
    <div class="body" ng-show="mode == 'list'">
        <div class="box no-border">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs hide-xs" style="border-bottom: 0px;">
                    <li ng-class="isFiltering == false ? 'active' : ''">
                        <a href="javascript:void(0)" style="cursor: pointer" ng-click="removeAllFilters()">
                            Tất cả bộ lọc
                        </a>
                    </li>
                    <li ng-class="isFiltering == true ? 'active' : ''" ng-show="isFiltering">
                        <a href="javascript:void(0)">
                            Tìm kiếm bộ lọc
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-body">
                <div class="input-group">
                    <!-- /btn-group -->
                    <input ng-model="keyword" type="text" class="form-control" ng-keyUp="$event.keyCode === 13 && searchFilter()"
                           placeholder="Tìm kiếm feed">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default btn-flat" ng-click="searchFilter()">
                            <i class="fa fa-search"></i>
                            Tìm kiếm
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <br>
                        <tbody>
                        <tr>
                            <th width="20%">Tên Feeds</th>
                            <th width="15%">T.gian cập nhật</th>
                            <th>Feed URL</th>
                            <th width="10%"></th>
                        </tr>
                        <tr class="product-item" ng-repeat="item in items" ng-show="items.length > 0">
                            <td ng-click="edit(item);">@{{item.name}}</td>
                            <td ng-click="edit(item);">@{{ summarizeDateTime(item.updated_at, true) }}</td>
                            <td>
                                <span class="s-tag" style="margin-top: 5px;font-size: 0.80em!important;">
                                    <a href="@{{item.url}}" target="_blank" style="color: inherit;">
                                        @{{item.url}}
                                    </a>
                                </span>
                                <button class="btn btn-default btn-sm pull-right" ngclipboard data-clipboard-text="@{{ item.url }}" title="Copy url"><i class="fa fa-copy"></i></button>
                            </td>
                            <td class="text-right">
                                <button class="btn btn-flat btn-danger" ng-click="deleteItem($index, item); $event.stopPropagation();">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                        <tr ng-show="items.length == 0">
                            <td colspan="4" class="text-center">Không có feed nào</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                @include('system.pagination')
            </div>
        </div>
    </div>
    @include('shopping-feed::setting.form');
</div>
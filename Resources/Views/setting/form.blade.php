<style>
    .chosen-single{height: 30px!important;}
    li.search-field input{height: 30px!important;}
    .chosen-container-single{width: 100%!important;}
    .chosen-single span {margin-top: 1px!important;}
</style>
<div class="row" ng-show="mode == 'create' || mode == 'update'">
    <div class="col-lg-10 col-lg-offset-1">
        <div class="back">
            <a href="javascript:;" ng-click="mode='list'" style="color: #637381">
                <i class="fa fa-chevron-left"></i>
                Danh sách feed
            </a>
        </div>
        <div>
            <div class="title">
                <h3 ng-show="mode == 'create'">Thêm Feed</h3>
                <h3 ng-show="mode == 'update'">Sửa Feed</h3>
            </div>
            <hr style="opacity: .2;"/>
            <div class="product-container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="title">
                            <h4>Cấu hình</h4>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="box no-border">
                            <form role="form">
                                <div class="box-body">
                                    <div class="form-group" ng-show="mode == 'update'">
                                        <label>Feed URL</label>
                                        <span class="s-tag" style="margin-top: 5px;">
                                            <a href="@{{feed.url}}" target="_blank" style="color: inherit;">
                                                @{{feed.url}}
                                            </a>
                                        </span>
                                        <b>Cập nhật lần cuối: @{{ summarizeDateTime(feed.updated_at, true) }}</b>
                                        <button class="btn btn-default btn-sm pull-right" ngclipboard data-clipboard-text="@{{ feed.url }}" title="Copy url"><i class="fa fa-copy"></i></button>
                                    </div>
                                    <div class="form-group">
                                        <label>Tên Feed *</label>
                                        <input type="text" ng-model="feed.name" class="form-control" placeholder="Tên feed"/>
                                    </div>
                                    <hr style="opacity: .2;"/>
                                    <div class="form-group">
                                        <label>Cấu hình sản phẩm</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionProduct" ng-model="feed.product_config"  id="optionsRadios1"  value="all" checked="">
                                                Tất cả sản phẩm
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionProduct" ng-model="feed.product_config"  id="optionsRadios2" value="category">
                                                Chọn theo danh mục
                                            </label>
                                        </div>
                                        <div ng-show="feed.product_config == 'category'">
                                            <select class="form-control chosen-select"
                                                    chosen
                                                    persistent-create-option="true"
                                                    skip-no-results="true"
                                                    ng-model="feed.category"
                                                    ng-options="category.name for category in categories track by category.id">
                                            </select>
                                        </div>
                                    </div>
                                    <hr style="opacity: .2;"/>
                                    <div class="form-group">
                                        <label>Google Product Taxonomy</label>
                                        <p>Danh mục sản phẩm của Google</p>
                                        <select class="form-control chosen-select"
                                                chosen
                                                persistent-create-option="true"
                                                skip-no-results="true"
                                                ng-model="feed.google_product_taxonomy"
                                                ng-options="category.product_taxonomy_name for category in googleProductTaxonomy track by category.id">
                                            <option value="">Không xác định</option>
                                        </select>
                                    </div>
                                    <hr style="opacity: .2;"/>
                                    <div class="form-group">
                                        <label>Cấu hình xuất bản sản phẩm biến thể</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionExportProduct" ng-model="feed.export_variant" id="optionsRadios1" value="all" checked="">
                                                Xuất bản tất cả biến thể của sản phẩm lên feed
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionExportProduct" ng-model="feed.export_variant" id="optionsRadios2" value="only">
                                                Xuất bản biến thể mặc định lên feed
                                            </label>
                                        </div>
                                    </div>
                                    <hr style="opacity: .2;"/>
                                    <div class="form-group">
                                        <label>Cấu hình tên sản phẩm có biến thể</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionTitleProduct" ng-model="feed.title_variant" id="optionsRadios1" value="no" checked="">
                                                Không thêm biến thể vào cuối tên sản phẩm
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="optionTitleProduct" ng-model="feed.title_variant" id="optionsRadios2" value="yes">
                                                Thêm biến thể như size, màu sắc ... vào cuối tên sản phẩm
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-footer">
            <button type="button" name="button" class="btn btn-flat btn-default pull-right" ng-click="mode='list'">Hủy</button>
            <button style="margin-right: 10px;" type="button" name="button" class="btn btn-flat pull-right btn-primary" id="btn-save-1" ng-click="save();">Lưu</button>
        </div>
    </div>
</div>
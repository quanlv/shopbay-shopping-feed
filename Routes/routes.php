<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['prefix' => 'shopping-feed', 'middleware' => 'example'], function () {
    Route::get('/', [
        'as' => 'shopping-feed::home',
        'uses' => 'ShoppingFeedController@index',
    ]);

    route::get('/{idFeedSetting}/{feedFileName}.xml', [
        'as' => 'shopping-feed::getfeed',
        'uses' => 'ShoppingFeedController@getGoogleShoppingFeed',
    ]);


});